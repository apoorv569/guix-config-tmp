;; NOTE: This file is generated from ~/System.org.  Please see commentary there.

(define-module (apoorv systems base)
  #:use-module (gnu)
  #:use-module (srfi srfi-1)
  #:use-module (gnu system nss)
  #:use-module (gnu services pm)
  #:use-module (gnu services desktop)
  #:use-module (gnu services docker)
  #:use-module (gnu services networking)
  #:use-module (gnu services virtualization)
  #:use-module (gnu services ssh)
  #:use-module (gnu services cups)
  #:use-module (gnu services guix)
  #:use-module (gnu packages cups)
  #:use-module (gnu packages wm)
  #:use-module (gnu packages vim)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages mtools)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages file-systems)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages gnuzilla)
  #:use-module (gnu packages web-browsers)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages package-management)
  #:use-module (nongnu packages linux)
  #:use-module (nongnu system linux-initrd))

(use-service-modules nix
                     desktop
                     networking
                     ssh
                     xorg)

(use-package-modules certs
                     shells
                     ssh)

(define %my-desktop-services
  (modify-services
   %desktop-services
   ;; Configure the substitute server for the Nonguix repo
   (guix-service-type config =>
                      (guix-configuration
                       (inherit config)
                       (substitute-urls
                        (append
                         (list "https://substitutes.nonguix.org")
                         %default-substitute-urls))
                       (authorized-keys
                        (append
                         (list
                          (plain-file "nonguix.pub" "(public-key
 (ecc
  (curve Ed25519)
  (q #C1FD53E5D4CE971933EC50C9F307AE2171A2D3B52C804642A7A35F84F3A4EA98#)
  )
 )"))
                         %default-authorized-guix-keys))))))

(define %xorg-libinput-config
  "Section \"InputClass\"
     Identifier \"Touchpads\"
     Driver \"libinput\"
     MatchDevicePath \"/dev/input/event*\"
     MatchIsTouchpad \"on\"

     Option \"Tapping\" \"on\"
     Option \"TappingDrag\" \"on\"
     Option \"DisableWhileTyping\" \"on\"
     Option \"MiddleEmulation\" \"on\"
     Option \"ScrollMethod\" \"twofinger\"
   EndSection

   Section \"InputClass\"
     Identifier \"Keyboards\"
     Driver \"libinput\"
     MatchDevicePath \"/dev/input/event*\"
     MatchIsKeyboard \"on\"
   EndSection")

(define-public base-operating-system
  (operating-system
   (host-name "guix")
   (timezone "Asia/Kolkata")
   (locale "en_US.utf8")

   ;; Use non-free Linux and firmware
   (kernel linux)
   (firmware (list linux-firmware))
   (initrd microcode-initrd)

   ;; Additional kernel modules
   ;; (kernel-loadable-modules (list v4l2loopback-linux-module))

   ;; Choose IN English keyboard layout.  The "altgr-intl"
   ;; variant provides dead keys for accented characters.
   (keyboard-layout (keyboard-layout "us" "altgr-intl"))

   ;; Use the UEFI variant of GRUB with the EFI System
   ;; Partition mounted on /boot/efi.
   (bootloader (bootloader-configuration
                ;; (bootloader grub-efi-bootloader)
                (bootloader grub-bootloader)
                (targets (list "/dev/vda"))
                (keyboard-layout keyboard-layout)))

   ;; Guix doesn't like it when there isn't a file-systems
   ;; entry, so add one that is meant to be overridden
   (file-systems (cons*
                  (file-system
                   (mount-point "/tmp")
                   (device "none")
                   (type "tmpfs")
                   (check? #f))
                  %base-file-systems))

   (groups (cons*
            (user-group
             (name "apoorv"))

            ;; Add the 'docker' group
            (user-group
             (system? #t)
             (name "docker"))

            ;; Add the 'realtime' group
            (user-group
             (system? #t)
             (name "realtime"))
            %base-groups))

   (users (cons
           (user-account
            (name "apoorv")
            (comment "Apoorv")
            (group "apoorv")
            (home-directory "/home/apoorv")
            (supplementary-groups '("wheel"     ;; sudo
                                    "netdev"    ;; network devices
                                    "kvm"
                                    "tty"
                                    "input"
                                    "docker"
                                    "realtime"  ;; Enable realtime scheduling
                                    "lp"        ;; control bluetooth devices
                                    "audio"     ;; control audio devices
                                    "video")))  ;; control video devices
           %base-user-accounts))

   ;; Install bare-minimum system packages
   (packages
    (append
     (map specification->package
          '("git"
            "exfat-utils"
            "fuse-exfat"
            "ntfs-3g"
            "neovim"
            "pipewire"
            "wireplumber"
            "nss-certs"      ;; for HTTPS access
            "gvfs"))         ;; for user mounts
     %base-packages))

   ;; Use the "desktop" services, which include the X11 log-in service,
   ;; networking with NetworkManager, and more
   (services
    (append
     (list
      ;; (service greetd-service-type)

      ;; Enable Slim login manager
      (service slim-service-type
               (slim-configuration
                (xorg-configuration
                 (xorg-configuration
                  (keyboard-layout keyboard-layout)
                  (extra-config (list %xorg-libinput-config))))))

      ;; Enable the ssh service
      (service openssh-service-type)

      ;; Enable the printing service
      ;; (service cups-service-type
      ;;          (cups-configuration
      ;;           (web-interface? #t)
      ;;           (extensions
      ;;            (list cups-filters))))

      ;; (set-xorg-configuration
      ;;  (xorg-configuration (keyboard-layout keyboard-layout)))

      ;; Enable JACK to enter realtime mode
      (pam-limits-service
       (list
        (pam-limits-entry "@realtime" 'both 'rtprio 99)
        (pam-limits-entry "@realtime" 'both 'nice -19)
        (pam-limits-entry "@realtime" 'both 'memlock 'unlimited)))

      ;; Enable /usr/bin/env in shell scripts
      (extra-special-file "/usr/bin/env"
                          (file-append coreutils "/bin/env"))

      ;; Enable Docker containers and virtual machines
      (service docker-service-type)
      (service libvirt-service-type
               (libvirt-configuration
                (unix-sock-group "libvirt")
                (tls-port "16555")))

      ;; Enable the build service for Nix package manager
      (service nix-service-type)

      ;; Remove GDM, we're using SLiM instead
      (remove (lambda (service)
                (eq? (service-kind service) gdm-service-type))))

     %my-desktop-services))

   ;; Allow resolution of '.local' host names with mDNS
   (name-service-switch %mdns-host-lookup-nss)))
