;; NOTE: This file is generated from ~/repos/org-files/Personal/Guix.org.  Please see commentary there.

(define-module (apoorv systems MachineY)
  #:use-module (apoorv systems base)
  #:use-module (apoorv utils)
  #:use-module (gnu)
  #:use-module (gnu home)
  #:use-module (gnu services)
  #:use-module (gnu system)
  #:use-module (gnu system uuid)
  #:use-module (gnu system file-systems)
  #:use-module (gnu system mapped-devices)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages file-systems)
  #:use-module (gnu packages xorg)
  #:use-module (gnu services linux)
  #:use-module (guix)
  #:use-module (guix transformations)
  #:use-module (nongnu packages linux)
  #:use-module (nongnu system linux-initrd))

(use-service-modules desktop
                     networking
                     ssh
                     xorg)

(define home
  (home-environment
   (packages (gather-manifest-packages '(audio_and_video
                                         cli-tools
                                         emacs
                                         language-tools-and-fonts
                                         music
                                         programming
                                         virtualization-and-containers
                                         games)))))
;; (services common-home-services)))

;; Allow members of the "video" group to change the screen brightness.
(define %backlight-udev-rule
  (udev-rule
   "90-backlight.rules"
   (string-append "ACTION==\"add\", SUBSYSTEM==\"backlight\", "
                  "RUN+=\"/run/current-system/profile/bin/chgrp video /sys/class/backlight/%k/brightness\""
                  "\n"
                  "ACTION==\"add\", SUBSYSTEM==\"backlight\", "
                  "RUN+=\"/run/current-system/profile/bin/chmod g+w /sys/class/backlight/%k/brightness\"")))

(define %my-desktop-services
  (modify-services %desktop-services
                   (elogind-service-type config =>
                                         (elogind-configuration
                                          (inherit config)
                                          (handle-lid-switch-external-power 'suspend)))
                   (udev-service-type config =>
                                      (udev-configuration
                                       (inherit config)
                                       (rules
                                        (cons %backlight-udev-rule
                                              (udev-configuration-rules config)))))))

(define system
  (operating-system
   (inherit base-operating-system)
   (host-name "MachineY")
   (firmware (cons* realtek-firmware
                    %base-firmware))

   (keyboard-layout (keyboard-layout "us" "altgr-intl" #:model "thinkpad"))

   (packages
    (append
     (map specification->package
          '("bluez"))
     %base-packages))

   ;; (mapped-devices
   ;;  (list (mapped-device
   ;;         (source (uuid "23196f9c-6207-414b-8686-a9c3fa969160"))
   ;;         (target "cryptroot")
   ;;         (type luks-device-mapping))))

   ;; (file-systems (cons*
   ;;                (file-system
   ;;                 (device "/dev/sda1")
   ;;                 (mount-point "/boot/efi")
   ;;                 (type "vfat"))
   ;;                (file-system
   ;;                 (device "/dev/sda2")
   ;;                 (mount-point "/")
   ;;                 (type "btrfs")
   ;;                 (options "rw,noatime,compress=zstd,ssd,space_cache=v2,subvolid=256,subvol=/@"))
   ;;                (file-system
   ;;                 (device "/dev/sda2")
   ;;                 (mount-point "/home")
   ;;                 (type "btrfs")
   ;;                 (options "rw,noatime,compress=zstd,ssd,space_cache=v2,subvolid=257,subvol=/@home"))
   ;;                (file-system
   ;;                 (device "/dev/sda2")
   ;;                 (mount-point "/.snapshots")
   ;;                 (type "btrfs")
   ;;                 (options "rw,noatime,compress=zstd,ssd,space_cache=v2,subvolid=258,subvol=/@snapshots"))
   ;;                (file-system
   ;;                 (device "/dev/sda2")
   ;;                 (mount-point "/var/log")
   ;;                 (type "btrfs")
   ;;                 (options "rw,noatime,compress=zstd,ssd,space_cache=v2,subvolid=259,subvol=/@var_log"))
   ;;                %base-file-systems))

   ;; (file-systems (cons* (file-system
   ;;                       (mount-point "/")
   ;;                       (device "/dev/mapper/cryptroot")
   ;;                       (type "ext4")
   ;;                       (dependencies mapped-devices))
   ;;                      (file-system
   ;;                       (mount-point "/boot/efi")
   ;;                       (device (uuid "5D7D-2B00"
   ;;                                     'fat32))
   ;;                       (type "vfat")) %base-file-systems))

   (file-systems (cons* (file-system
                         (mount-point "/")
                         (device (uuid
                                  "b2ba2357-b538-465b-a3fd-b084bcae3562"
                                  'ext4))
                         (type "ext4")) %base-file-systems))

   (services
    (append
     (list
      ;; Power and thermal management services
      ;; (service thermald-service-type)
      ;; (service tlp-service-type
      ;;          (tlp-configuration
      ;;           (cpu-boost-on-ac? #t)
      ;;           (wifi-pwr-on-bat? #t)))

      ;; Enable the bluetooth service
      (bluetooth-service #:auto-enable? #t))

     ;; %my-desktop-services

     %desktop-services))))

(if (getenv "RUNNING_GUIX_HOME") home system)
